package com.babylonapp

import android.support.test.espresso.Espresso
import android.support.test.espresso.ViewInteraction
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers

internal fun ViewInteraction.isDisplayed() = check(matches(ViewMatchers.isDisplayed()))

internal fun Int.isDisplayed() = Espresso.onView(ViewMatchers.withId(this)).isDisplayed()

internal fun ViewInteraction.withText(text: String) = check(matches(ViewMatchers.withText(text)))
