package com.babylonapp

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.RecyclerView
import com.babylonapp.presentation.list.MainActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class DetailsActivityTest {

    @get:Rule
    var testRule: ActivityTestRule<MainActivity> = ActivityTestRule(MainActivity::class.java)

    @Test
    fun showPostsDetails() {
        // This should be prepared with mock data to have always same result
        // for now I will trust the backend returns always same posts
        goToSecondPostItem()

        R.id.post_title.isDisplayed().withText("qui est esse")
        R.id.user_name.isDisplayed().withText("Bret")
        R.id.post_comments_counter.isDisplayed().withText("5 comments")
        R.id.post_body.isDisplayed()
    }

    private fun goToSecondPostItem() {
        onView(withId(R.id.post_list_view))
            .perform(actionOnItemAtPosition<RecyclerView.ViewHolder>(2, click()))
    }

}
