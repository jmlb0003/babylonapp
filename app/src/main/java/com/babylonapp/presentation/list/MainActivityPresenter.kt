package com.babylonapp.presentation.list

import com.babylonapp.presentation.base.BasePresenter
import com.babylonapp.presentation.base.BasePresenterImp
import javax.inject.Inject

class MainActivityPresenter
@Inject constructor() : BasePresenterImp<MainActivityPresenter.MainActivityView>() {

    fun onShowLoadingFromPostListFragment() {
        getView()?.showLoading()
    }

    fun onHideLoadingFromPostListFragment() {
        getView()?.hideLoading()
    }

    interface MainActivityView : BasePresenter.BaseView {

        fun showLoading()

        fun hideLoading()

    }

}
