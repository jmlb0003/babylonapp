package com.babylonapp.presentation.list.adapter

import com.babylonapp.domain.model.Post

sealed class PostListItem {

    object Header : PostListItem()

    class PostItem(val post: Post) : PostListItem()

}
