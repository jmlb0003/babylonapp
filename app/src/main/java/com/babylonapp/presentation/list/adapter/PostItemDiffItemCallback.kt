package com.babylonapp.presentation.list.adapter

import android.support.v7.util.DiffUtil

object PostItemDiffItemCallback : DiffUtil.ItemCallback<PostListItem>() {

    override fun areItemsTheSame(oldItem: PostListItem, newItem: PostListItem) =
        when (oldItem) {
            is PostListItem.Header -> newItem is PostListItem.Header
            is PostListItem.PostItem -> newItem is PostListItem.PostItem
        }

    override fun areContentsTheSame(oldItem: PostListItem, newItem: PostListItem) =
        oldItem == newItem

}
