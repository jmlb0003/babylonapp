package com.babylonapp.presentation.list

import com.babylonapp.app.utils.ErrorHandler
import com.babylonapp.app.utils.Schedulers
import com.babylonapp.domain.PostsRepository
import dagger.Module
import dagger.Provides

@Module
class PostListModule {

    @Provides
    fun providePostListPresenter(
        schedulers: Schedulers,
        errorHandler: ErrorHandler,
        repository: PostsRepository
    ): PostListPresenter =
        PostListPresenter(schedulers, errorHandler, repository)

}
