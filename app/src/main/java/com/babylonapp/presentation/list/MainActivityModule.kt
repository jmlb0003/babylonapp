package com.babylonapp.presentation.list

import dagger.Module
import dagger.Provides

@Module
class MainActivityModule {

    @Provides
    fun provideMainActivityPresenter(): MainActivityPresenter = MainActivityPresenter()

}
