package com.babylonapp.presentation.base

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

abstract class BaseActivity<V : BasePresenter.BaseView, P : BasePresenter<V>> : AppCompatActivity(),
    BasePresenter.BaseView {

    protected abstract fun getPresenter(): P

    protected abstract fun getMVPViewReference(): V

    protected abstract fun initializeInjector()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initializeInjector()
        getPresenter().attachView(getMVPViewReference())
    }

    override fun onPause() {
        super.onPause()
        getPresenter().attachView(getMVPViewReference())
    }

}
