package com.babylonapp.presentation.base

import com.babylonapp.app.utils.ErrorHandler
import io.reactivex.disposables.CompositeDisposable
import java.lang.ref.WeakReference

abstract class BasePresenterImp<V : BasePresenter.BaseView>(
    protected open val errorHandler: ErrorHandler? = null
) : BasePresenter<V> {

    protected val disposables = CompositeDisposable()

    private var referenceView: WeakReference<V>? = null

    override fun getView(): V? =
        if (referenceView != null) {
            referenceView?.get()
        } else {
            errorHandler?.handleViewNullError(javaClass.simpleName)
            null
        }

    override fun attachView(view: V) {
        referenceView = WeakReference(view)
    }

    override fun detachView() {
        getView()?.let {
            referenceView!!.clear()
            referenceView = null
        }
        disposables.clear()
    }
}
