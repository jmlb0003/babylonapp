package com.babylonapp.presentation.details

import com.babylonapp.app.utils.ErrorHandler
import com.babylonapp.app.utils.TasksSchedulers
import com.babylonapp.domain.PostsRepository
import com.babylonapp.domain.model.Comment
import com.babylonapp.domain.model.Post
import com.babylonapp.domain.model.User
import com.babylonapp.presentation.base.BasePresenter
import com.babylonapp.presentation.base.BasePresenterImp
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import io.reactivex.rxkotlin.plusAssign
import javax.inject.Inject

class DetailsPresenter
@Inject constructor(
    private val schedulers: TasksSchedulers,
    override val errorHandler: ErrorHandler,
    private val repository: PostsRepository
) : BasePresenterImp<DetailsPresenter.DetailsView>(errorHandler) {

    fun onViewInitialized(post: Post) {
        disposables += requestPostsDetails(post)
            .subscribeOn(schedulers.getBackgroundThread())
            .observeOn(schedulers.getUiThread())
            .doOnSubscribe {
                getView()?.showTitle(post.title)
                getView()?.showBody(post.body)
                getView()?.showLoading()
            }
            .doAfterTerminate { getView()?.hideLoading() }
            .subscribe({ (user, comments) ->
                displayPostDetails(user, comments)
            }, ::handleErrorResult)


    }

    private fun requestPostsDetails(post: Post) =
        Single.zip(
            repository.getUser(post.userId),
            repository.getComments(post.postId),
            BiFunction { user: User?, comments: List<Comment> ->
                user to comments
            })

    private fun displayPostDetails(user: User?, comments: List<Comment>) {
        getView()?.displayOtherDetails(user?.userName, comments.size)
    }

    private fun handleErrorResult(error: Throwable) {
        errorHandler.handleNetworkResponseError(javaClass.simpleName, error)
        getView()?.displayError(error.message ?: "")
    }

    interface DetailsView : BasePresenter.BaseView {

        fun showLoading()

        fun hideLoading()

        fun showTitle(title: String)

        fun showBody(body: String)

        fun displayOtherDetails(userName: String?, commentCount: Int)

        fun displayError(s: String)

    }

}
