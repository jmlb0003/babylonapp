package com.babylonapp.presentation.details

import com.babylonapp.app.utils.ErrorHandler
import com.babylonapp.app.utils.Schedulers
import com.babylonapp.domain.PostsRepository
import dagger.Module
import dagger.Provides

@Module
class DetailsModule {

    @Provides
    fun provideDetailsPresenter(
        schedulers: Schedulers,
        errorHandler: ErrorHandler,
        repository: PostsRepository
    ): DetailsPresenter =
        DetailsPresenter(schedulers, errorHandler, repository)

}
