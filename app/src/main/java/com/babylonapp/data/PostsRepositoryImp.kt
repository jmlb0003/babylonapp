package com.babylonapp.data

import com.babylonapp.domain.PostsMemoryDataSource
import com.babylonapp.domain.PostsNetworkDataSource
import com.babylonapp.domain.PostsRepository
import com.babylonapp.domain.model.Post
import com.babylonapp.domain.model.User
import io.reactivex.Single
import javax.inject.Inject

class PostsRepositoryImp
@Inject constructor(
    private val networkDataSource: PostsNetworkDataSource,
    private val memoryDataSource: PostsMemoryDataSource
) : PostsRepository {

    override fun getPosts(): Single<List<Post>> =
        Single.concat(memoryDataSource.getPosts(), getPostFromNetworkDataSource())
            .filter { posts -> posts.isNotEmpty() }
            .first(emptyList())

    override fun getUser(userId: Int) =
        Single.concat(memoryDataSource.getUsers(), getUsersFromNetworkDataSource())
            .filter { users -> users.isNotEmpty() }
            .first(emptyList())
            .map { rawUsers: List<User> ->
                rawUsers.firstOrNull { user -> user.id == userId }
            }

    override fun getComments(postId: Int) =
        Single.concat(memoryDataSource.getComments(), getCommentsFromNetworkDataSource())
            .filter { comments -> comments.isNotEmpty() }
            .first(emptyList())
            .flatMap { rawComments ->
                Single.just(rawComments.filter { comment -> comment.postId == postId })
            }

    private fun getPostFromNetworkDataSource() =
        networkDataSource.getPosts()
            .doAfterSuccess { posts ->
                memoryDataSource.cachePosts(posts)
            }

    private fun getUsersFromNetworkDataSource() =
        networkDataSource.getUsers()
            .doAfterSuccess { users ->
                memoryDataSource.cacheUsers(users)
            }

    private fun getCommentsFromNetworkDataSource() =
        networkDataSource.getComments()
            .doAfterSuccess { comments ->
                memoryDataSource.cacheComments(comments)
            }

}
