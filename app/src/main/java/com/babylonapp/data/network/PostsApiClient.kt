package com.babylonapp.data.network

import com.babylonapp.data.network.model.NetworkComment
import com.babylonapp.data.network.model.NetworkPost
import com.babylonapp.data.network.model.NetworkUser
import retrofit2.Call
import retrofit2.http.GET

interface PostsApiClient {

    @GET("posts")
    fun getPosts(): Call<List<NetworkPost>>

    @GET("users")
    fun getUsers(): Call<List<NetworkUser>>

    @GET("comments")
    fun getComments(): Call<List<NetworkComment>>

}
