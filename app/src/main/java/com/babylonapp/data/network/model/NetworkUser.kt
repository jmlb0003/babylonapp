package com.babylonapp.data.network.model

import com.google.gson.annotations.SerializedName

data class NetworkUser(
    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String,
    @SerializedName("username") val userName: String,
    @SerializedName("email") val email: String
)
