package com.babylonapp.app

import android.app.Application
import com.babylonapp.app.di.AppComponent
import com.babylonapp.app.di.DaggerAppComponent

class BabylonApplication : Application() {

    val component: AppComponent by lazy {
        DaggerAppComponent.create()
    }

}
