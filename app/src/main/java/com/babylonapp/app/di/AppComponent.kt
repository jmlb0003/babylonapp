package com.babylonapp.app.di

import com.babylonapp.presentation.details.DetailsModule
import com.babylonapp.presentation.list.MainActivityModule
import com.babylonapp.presentation.list.PostListModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class,
        RepositoryModule::class
    ]
)
interface AppComponent {

    fun mainActivityComponent(module: MainActivityModule): MainActivityComponent

    fun postListComponent(module: PostListModule): PostListComponent

    fun detailsComponent(module: DetailsModule): DetailsComponent

}
