package com.babylonapp.app.di

import com.babylonapp.presentation.list.PostListFragment
import com.babylonapp.presentation.list.PostListModule
import dagger.Subcomponent

@Subcomponent(
    modules = [
        PostListModule::class
    ]
)
interface PostListComponent {
    fun inject(fragment: PostListFragment)
}
