package com.babylonapp.app.di

import com.babylonapp.presentation.list.MainActivity
import com.babylonapp.presentation.list.MainActivityModule
import dagger.Subcomponent

@Subcomponent(
    modules = [
        MainActivityModule::class
    ]
)
interface MainActivityComponent {
    fun inject(activity: MainActivity)
}
