package com.babylonapp.domain.model

data class User(
    val id: Int,
    val name: String,
    val userName: String
)
