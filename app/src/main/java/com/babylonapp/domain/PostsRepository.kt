package com.babylonapp.domain

import com.babylonapp.domain.model.Comment
import com.babylonapp.domain.model.Post
import com.babylonapp.domain.model.User
import io.reactivex.Single

interface PostsRepository {

    fun getPosts(): Single<List<Post>>

    fun getUser(userId: Int): Single<User?>

    fun getComments(postId: Int): Single<List<Comment>>

}
