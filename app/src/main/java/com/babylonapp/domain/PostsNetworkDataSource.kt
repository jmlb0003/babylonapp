package com.babylonapp.domain

import com.babylonapp.domain.model.Comment
import com.babylonapp.domain.model.Post
import com.babylonapp.domain.model.User
import io.reactivex.Single

interface PostsNetworkDataSource {

    fun getPosts(): Single<List<Post>>

    fun getUsers(): Single<List<User>>

    fun getComments(): Single<List<Comment>>

}
